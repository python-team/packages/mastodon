requests>=2.4.2
python-dateutil
decorator>=4.0.0
blurhash>=1.1.4

[:platform_system != "Windows"]
python-magic

[:platform_system == "Windows"]
python-magic-bin

[blurhash]
blurhash>=1.1.4

[docs]
sphinx-rtd-theme

[test]
pytest
pytest-cov
vcrpy
pytest-vcr
pytest-mock
requests-mock
pytz
pytest-retry

[test_old]
pytest
pytest-cov
vcrpy
pytest-vcr
pytest-mock
requests-mock
pytz

[webpush]
http_ece>=1.0.5
cryptography>=1.6.0
